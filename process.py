import sys
import copy
import time
import Queue
from threading import Thread

class Process(Thread):

    messages_file = open('message.out', 'w', 0)
    critical_section_file = open('critical_section.out', 'w', 0)
    neighbors = {}
    finished_procs = 0

    def __init__(self, id):
        Thread.__init__(self, name=id)
        self.id = id
        self.q = Queue.Queue()
        self.requests = {}
        self.pending = {}
        self.clock = 1
        self.req_stamp = 0
        self.permissions = set()
        self.sent_request_counter = 0
        self.crit_counter = 0
        self.kill_perm_count = 0
        self.has_finished = False
        Process.neighbors[self.id] =  self
        Process.messages_file.write('participant ' + str(self.id) + '\n')

    def initialize(self):
        self.requests = copy.copy(Process.neighbors)
        del self.requests[self.id]

    def run(self):
        self.initialize()
        while(True):
            if (bool(self.requests)):
                if self.sent_request_counter < len(self.requests):
                    for key in self.requests:
                        Process.messages_file.write(str(self.id) + '->' + str(key) + ': REQUEST, clock=' + str(self.clock)  + '\n')
                        message = {}
                        message['type'] = 'request'
                        message['ts'] = self.clock
                        message['id'] = self.id
                        self.requests[key].q.put(message)
                        self.sent_request_counter += 1
                    self.req_stamp = self.clock
                    self.counter = 0
            else:
                self._critical_section()
            while not self.q.empty():
                msg_rcv = self.q.get();
                if msg_rcv['type'] == 'request':
                    self.process_request(msg_rcv['ts'], msg_rcv['id'])
                elif msg_rcv['type'] == 'permission':
                    self.process_permission(msg_rcv['id'])
                elif msg_rcv['type'] == 'kill_permission':
                    self.permit_kill(msg_rcv['id'])

    def _critical_section(self):
        Process.messages_file.write('Note over ' + str(self.id)  + ': CRITICAL SECTION, clock=' + str(self.clock) + ', req_stamp=' + str(self.req_stamp) + '\n')
        self.crit_counter += 1
        for it in range(10):
            Process.critical_section_file.write('WRITING : ' + str(self.id) + ':' + `it` + '\n')
        Process.critical_section_file.write('-------------\n')
        if self.crit_counter >= 10: # Terminou a execucao
            self.kill()
        for key in self.pending:
            Process.messages_file.write(str(self.id) + '-->' + str(key) + ': PERMISSION\n')
            message = {}
            message['type'] = 'permission'
            message['id'] = self.id
            self.pending[key].q.put(message)
        self.req_stamp = 0
        self.pending = {}
        self.permissions = set()
        self.sent_request_counter = 0

    def process_request(self, clock, id):
        self.clock = max(self.clock, clock + 1)
        if self.req_stamp == 0 or clock < self.req_stamp or (clock == self.req_stamp and id < self.id):
            Process.messages_file.write(str(self.id) + '-->' + str(id) + ': PERMISSION\n')
            message = {}
            message['type'] = 'permission'
            message['id'] = self.id
            self.neighbors[id].q.put(message)
            self.requests[id] = Process.neighbors[id]
        else:
            self.pending[id] = Process.neighbors[id]

    def process_permission(self, id):
        self.permissions.add(id)
        if self.has_permission():
            self._critical_section()

    def kill(self):
        self.has_finished = True
        for key in self.pending:
            Process.messages_file.write(str(self.id) + '-->' + str(key) + ': PERMISSION\n')
            message = {}
            message['type'] = 'permission'
            message['id'] = self.id
            self.requests[key].q.put(message)
        for key in self.requests:
            Process.messages_file.write(str(self.id) + '->' + str(key) + ': KILL REQUEST\n')
            message = {}
            message['type'] = 'kill_permission'
            message['id'] = self.id
            self.requests[key].q.put(message)
        while(True):
            if not bool(self.requests):
                Process.finished_procs += 1
                del Process.neighbors[self.id]
                Process.messages_file.write('Note over ' + str(self.id)  + ': FIM DO PROCESSO\n')
                sys.exit(0)
            while not self.q.empty():
                msg_rcv = self.q.get();
                if msg_rcv['type'] == 'request':
                    Process.messages_file.write(str(self.id) + '-->' + str(msg_rcv['id']) + ': PERMISSION\n')
                    message = {}
                    message['type'] = 'permission'
                    message['id'] = self.id
                    self.neighbors[msg_rcv['id']].q.put(message)
                elif msg_rcv['type'] == 'kill_permission':
                    if(msg_rcv['id'] < self.id):
                        self.permit_kill(msg_rcv['id'])
                elif msg_rcv['type'] == 'permit_kill':
                    self.kill_perm_count += 1
                    if self.kill_perm_count >= len(Process.neighbors) - 1:
                        Process.finished_procs += 1
                        del Process.neighbors[self.id]
                        Process.messages_file.write('Note over ' + str(self.id)  + ': FIM DO PROCESSO\n')
                        sys.exit(0)

    def permit_kill(self, id):
        del self.requests[id]
        Process.messages_file.write(str(self.id) + '-->' + str(id) + ': KILL PERMISSION\n')
        message = {}
        message['type'] = 'permit_kill'
        message['id'] = self.id
        self.neighbors[id].q.put(message)

    def has_permission(self):
        permission = True
        for key in self.requests:
            if key not in self.permissions:
                permission = False
        return permission
