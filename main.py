#!/usr/bin/python

from process import Process

if __name__ == "__main__":

    procs = list()

    for x in xrange(5):
        procs.append(Process(x))

    map(lambda proc: proc.start(), procs)
