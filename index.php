<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Mutual Exclusion">

    <script type="text/javascript" src="js/underscore-min.js"></script>
    <script type="text/javascript" src="js/raphael-min.js"></script>
    <script type="text/javascript" src="js/sequence-diagram-min.js"></script>

    <title>Mutual Exclusion</title>
  </head>
  <body>
    <div id="file-div">
      <input id="file" type="file">
    </div>
    <div id="diagram">
      <script>
        document.getElementById('file').onchange = function(){
         var file = this.files[0];
          var reader = new FileReader();
          reader.onload = function(progressEvent){
            var diagram = Diagram.parse(this.result);
            diagram.drawSVG("diagram", {theme: 'simple'});
          };
        reader.readAsText(file);
        };
      </script>
    </div>
  </body>
</html>
